import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutes } from './constants/app-routes';
import { LoginLayoutComponent } from 'projects/pro-library/src/public-api';

const routes: Routes = [
  {
    path: '',
    redirectTo: AppRoutes.LOGIN,
    pathMatch: 'full',
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      { path: '', loadChildren: () =>
  import('projects/system/src/app/modules/login/login.module').then(m => m.LoginModule) }
    ]
  },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
