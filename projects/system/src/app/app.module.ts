import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from 'projects/pro-library/src/lib/core';

import { AppComponent } from './app.component';
import { AppHeaderComponent, AppFooterComponent, AppSidebarComponent, AppSidebarNavComponent, AppSidebarFooterComponent } from 'projects/pro-library/src/lib/shared/components';
import { FullLayoutComponent, SimpleLayoutComponent, LoginLayoutComponent, GeneralLayoutComponent } from 'projects/pro-library/src/lib/shared/containers';
import { AppSidebarHeaderComponent } from './components';
import { SharedModule } from './modules/shared/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CSApiConstants } from 'projects/pro-library/src/lib/shared/constants';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

const APP_COMPONENTS = [
  AppHeaderComponent,
  AppFooterComponent,
  AppSidebarComponent,
  AppSidebarNavComponent,
  AppSidebarFooterComponent,
  AppSidebarHeaderComponent,
];

const APP_CONTAINERS = [
  FullLayoutComponent,
  SimpleLayoutComponent,
  LoginLayoutComponent,
  GeneralLayoutComponent
];

@NgModule({
  declarations: [
    AppComponent,
    ...APP_COMPONENTS,
    ...APP_CONTAINERS
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    NgxPermissionsModule.forRoot(),

  ],
  providers: [
    {
      provide: 'endpoint',
      useValue: CSApiConstants.SYSTEM_PATH
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
