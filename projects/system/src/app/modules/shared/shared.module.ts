import { NgModule } from '@angular/core';
import { SharedModule as LibSharedModule } from 'projects/pro-library/src/public-api';

@NgModule({
  declarations: [],
  imports: [
    LibSharedModule
  ],
  exports: [
    LibSharedModule
  ],
})
export class SharedModule { }
