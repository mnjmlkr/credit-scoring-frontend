import { BehaviorSubject, Subject } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { UserProfile } from '../../models';
import { PermissionService } from 'projects/pro-library/src/lib/core/services/permission.service';
import { GenericResponse } from 'projects/pro-library/src/lib/shared/models';
import { ApiConstants } from 'projects/system/src/app/constants/api-constants';


@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    userProfile: UserProfile;
    userProfileRxSubject: Subject<UserProfile> = new BehaviorSubject<UserProfile>(this.userProfile);

    constructor(
        private http: HttpClient,
        private permissionsService: PermissionService,
        private router: Router,
    ) { }

    getAndSyncProfileDetail() {
        this.http.get<any>(ApiConstants.generateWebPath(ApiConstants.PROFILE))
            .subscribe((successResponse: GenericResponse<UserProfile>) => {
                this.userProfile = successResponse.data;

                this.permissionsService.setPermissions(this.userProfile.roles);
                this.syncProfile();
            }, (errorResponse) => {
                console.log(errorResponse.error.message);
            });
    }

    syncProfile() {
        this.userProfileRxSubject.next(
            this.userProfile
        );
    }

    getProfileRxSubject() {
        return this.userProfileRxSubject.asObservable();
    }

}
