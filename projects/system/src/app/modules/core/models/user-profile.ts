import { Group } from './group';

export class UserProfile {
  id: string;
  fullName: string;
  firstName: string;
  address: string;
  mobileNumber: string;
  email: string;
  status: number;
  roles: string[];
  profilePicture: string;
  group: Group;
}
