import { NgModule } from '@angular/core';
import { CoreModule as LibCoreModule } from 'projects/pro-library/src/public-api';

@NgModule({
  declarations: [],
  imports: [
    LibCoreModule
  ],
  exports: [
    LibCoreModule
  ]
})
export class CoreModule { }
