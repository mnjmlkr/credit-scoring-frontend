export class RoutingConstants {
    /**
     * Root
     */
    static readonly LOGIN = 'login';
    static readonly DASHBOARD = 'dashboard';

    /**
     * Dynamic Routes
     */
    static readonly ID = '/:id';

    /**
     * Errors
     */
    static readonly SERVER_ERROR = 'server-error';
    static readonly NOT_FOUND = 'page-not-found';

    /**
     * @returns string - path seperated by '/'
     */
    static generatePath(...sections: string[]): string {
        return sections.reduce((path, section) => `${path}/${section}`, '');
    }
}
