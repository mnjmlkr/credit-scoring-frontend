import { Component, OnInit, ChangeDetectorRef, AfterContentChecked, OnDestroy } from '@angular/core';
import { LoginModel } from '../shared/models';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LoginFormService } from '../shared/form/login-form.service';
import { AuthenticationService } from 'projects/pro-library/src/lib/core/services/authentication.service';
import { CredentialsService } from 'projects/pro-library/src/lib/core/services/credentials.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppRoutes } from '../../../constants/app-routes';
import { GenericResponse } from 'projects/pro-library/src/lib/shared/models';
import { ProfileService } from '../../core';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterContentChecked, OnDestroy {

    loginModel: LoginModel = new LoginModel();
    loginForm: FormGroup;

    showPassword = false;
    buttonClicked = false;
    disabled = true;
    userSubscription: Subscription;

    private redirectTo: string;

    constructor(
        private loginFormService: LoginFormService,
        private authService: AuthenticationService,
        private credentialService: CredentialsService,
        private router: Router,
        private route: ActivatedRoute,
        private cdr: ChangeDetectorRef,
        private profileService: ProfileService

    ) { }

    ngOnInit(): void {
        this.loginForm = this.loginFormService.loginForm(this.loginModel);
        console.log('login', this.loginForm);
        console.log(this.credentialService.credentials);
        if (this.credentialService.isAuthenticated()) {
            this.router.navigate([AppRoutes.DASHBOARD]);
        }
        this.route.queryParams.subscribe((params) => {
            if (params && params.redirect) {
                this.redirectTo = params.redirect;
            }
        });
    }

    get f() { return this.loginForm.controls; }


    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }
    logIn() {
        console.log('here');
        if (!this.loginForm.valid) {
            return void 0;
        }
        this.buttonClicked = true;
        this.authService.login(this.loginForm.value).subscribe((successResponse: GenericResponse) => {
            if (successResponse.success) {
                this.profileService.getAndSyncProfileDetail();
                this.userSubscription = this.profileService.getProfileRxSubject().subscribe((userProfile) => {
                    if (!this.redirectTo || !userProfile) {
                        this.router.navigate([AppRoutes.DASHBOARD]);
                    } else {
                        this.router.navigate([decodeURIComponent(this.redirectTo)]);
                    }
                }, (error) => {
                    console.log('Something went wrong. Please contact admin.');
                    this.router.navigate([AppRoutes.DASHBOARD]);
                });
            }
        }, (error: HttpErrorResponse) => {
            this.buttonClicked = false;
            if (error.error && error.error.data) {
                console.log(error.error.data);
            }
            console.log(error.error.message);
        });
    }

    ngOnDestroy() {
        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }
    }

}
