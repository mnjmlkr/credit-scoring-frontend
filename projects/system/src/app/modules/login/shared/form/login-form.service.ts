import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class LoginFormService {

  constructor(
    private fb: FormBuilder
) { }

  public loginForm(loginModel: LoginModel) {
    return this.fb.group({
        email: [loginModel.email, [Validators.required]],
        password: [loginModel.password, [Validators.required]],
    });
}
}
