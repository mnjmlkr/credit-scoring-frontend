import { CSApiConstants } from 'projects/pro-library/src/lib/shared/constants';

export class ApiConstants {

  public static LOGIN = CSApiConstants.LOGIN;
  public static AUTH = CSApiConstants.AUTH;
  public static SYSTEM_PATH = CSApiConstants.SYSTEM_PATH;

  public static SYSTEM = 'system';
  public static PROFILE = 'profile';

  static generateWebPath(...sections: string[]) {
    return sections.reduce((path, section) => `${path}/${section}`, this.SYSTEM_PATH);
  }
}
