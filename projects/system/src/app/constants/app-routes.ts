import { CSAppRoutesConstants } from 'projects/pro-library/src/lib/shared/constants';

export class AppRoutes {
    static LOGIN = CSAppRoutesConstants.LOGIN;
    static DASHBOARD = CSAppRoutesConstants.DASHBOARD;
    static PAGE_NOT_FOUND = CSAppRoutesConstants.PAGE_NOT_FOUND;
}
