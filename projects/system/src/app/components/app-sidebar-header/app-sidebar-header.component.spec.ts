import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSidebarHeaderComponent } from './app-sidebar-header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProfileService } from '../../modules/core';
import { NgxPermissionsModule, USE_PERMISSIONS_STORE, NgxPermissionsService, NgxPermissionsStore } from 'ngx-permissions';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from '../../constants/app-routes';

describe('AppSidebarHeaderComponent', () => {
    let component: AppSidebarHeaderComponent;
    let fixture: ComponentFixture<AppSidebarHeaderComponent>;
    const router = {
        navigate: jasmine.createSpy('navigate')
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                ToastrModule.forRoot(),
                NoopAnimationsModule,
                NgxPermissionsModule
            ],
            declarations: [AppSidebarHeaderComponent],
            providers: [
                ProfileService,
                {
                    provide: USE_PERMISSIONS_STORE
                },
                NgxPermissionsService,
                NgxPermissionsStore,
                { provide: Router, useValue: router },
            ],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppSidebarHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should navigate to profile when profile button is triggered', () => {
        component.onViewProfile();
        expect(router.navigate).toHaveBeenCalledWith([AppRoutes.generateAppPath(AppRoutes.PROFILE)]);
    });

    it('should navigate to change password when password button is triggered', () => {
        component.onChangePassword();
        expect(router.navigate).toHaveBeenCalledWith([AppRoutes.CHANGE_PASSWORD]);
    });

    it('should emit event when logout button is triggered', () => {
        spyOn(component.logout, 'emit');

        component.onLogout();
        expect(component.logout.emit).toHaveBeenCalled();
    });
});
