import { TestBed, inject } from '@angular/core/testing';

import { NavigationService } from './navigation.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('NavigationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigationService],
      imports: [ HttpClientTestingModule ]
    });
  });

  it('should be created', inject([NavigationService], (service: NavigationService) => {
    expect(service).toBeTruthy();
  }));
});
