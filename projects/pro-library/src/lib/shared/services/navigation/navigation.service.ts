import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NavigationModel } from '../../models/navigation.model';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class NavigationService {
  constructor(private http: HttpClient) {}

  getAllNavigation(): Observable<NavigationModel[]> {
    return this.http.get('assets/nav.json')
    .pipe(map((data: any[]) =>
      data.map((v: NavigationModel) => {
        let children: NavigationModel[] | null = null;
        if (v.children && v.children.length) {
          children = v.children.map((c: NavigationModel) => {
            if (!c.anonymous) { c.role.push('ROLE_SUPER_ADMIN'); }
            return new NavigationModel().deserialize(c);
          });
          v.children = [ ... children ];
        }
        if (!v.anonymous) { v.role.push('ROLE_SUPER_ADMIN'); }
        return new NavigationModel().deserialize(v);
      })
    ));
  }
}
