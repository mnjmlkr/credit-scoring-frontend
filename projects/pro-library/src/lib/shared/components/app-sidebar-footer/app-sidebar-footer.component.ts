import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'lib-sidebar-footer',
    templateUrl: './app-sidebar-footer.component.html',
    styleUrls: ['./app-sidebar-footer.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppSidebarFooterComponent {

}
