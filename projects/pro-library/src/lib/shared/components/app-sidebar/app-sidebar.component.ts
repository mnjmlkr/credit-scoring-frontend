import {
    Component,
    ViewEncapsulation,
} from '@angular/core';

@Component({
    selector: 'lib-sidebar',
    templateUrl: './app-sidebar.component.html',
    styleUrls: ['./app-sidebar.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
/* istanbul ignore file */
export class AppSidebarComponent{

}
