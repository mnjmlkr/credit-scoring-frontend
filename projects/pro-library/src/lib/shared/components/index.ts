import {  AppFooterComponent } from './app-footer/app-footer.component';
import {  AppHeaderComponent } from './app-header/app-header.component';
import {  AppSidebarComponent } from './app-sidebar/app-sidebar.component';
import {  AppSidebarFooterComponent } from './app-sidebar-footer/app-sidebar-footer.component';
import {  AppSidebarNavComponent } from './app-sidebar-nav/app-sidebar-nav.component';

export {
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarNavComponent,
};

export default {
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarNavComponent,
};
