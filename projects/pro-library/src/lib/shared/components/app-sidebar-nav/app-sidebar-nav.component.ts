import {
    Component,
    ViewEncapsulation,
} from '@angular/core';

@Component({
    selector: 'lib-sidebar-nav',
    templateUrl: './app-sidebar-nav.component.html',
    styleUrls: ['./app-sidebar-nav.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppSidebarNavComponent {

}
export const APP_SIDEBAR_NAV = [
    AppSidebarNavComponent
];


