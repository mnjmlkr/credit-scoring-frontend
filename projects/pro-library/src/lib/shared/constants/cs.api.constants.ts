import { environment } from 'projects/pro-library/src/environments/environment';

// @dynamic
export class CSApiConstants {
    public static SYSTEM_PATH = environment.SYSTEM_API;
    public static PATH = environment.SYSTEM_API;

    public static AUTH = 'auth';
    public static LOGIN = 'login';
    public static LOGOUT = 'logout';

    static generateWebPath(...sections: string[]) {
        return sections.reduce((path, section) => `${path}/${section}`);
    }
}
