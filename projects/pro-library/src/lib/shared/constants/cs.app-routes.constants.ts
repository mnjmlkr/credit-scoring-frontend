import { environment } from 'projects/pro-library/src/environments/environment';

// @dynamic
export class CSAppRoutesConstants {

    public static SYSTEM_PORTAL = environment.SYSTEM_PORTAL;

    public static LOGIN = 'login';
    public static DASHBOARD = 'dashboard';
    public static LOGOUT = 'logout';

    public static PAGE_NOT_FOUND = '404';

    static generateAppPath(...sections: string[]) {
        return sections.reduce((path, section) => `${path}/${section}`);
    }
}
