import { Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'lib-root',
    templateUrl: './full-layout.component.html',
    styleUrls: ['./full-layout.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FullLayoutComponent {
}
