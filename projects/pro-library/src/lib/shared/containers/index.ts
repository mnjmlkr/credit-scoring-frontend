
import { FullLayoutComponent } from './full-layout/full-layout.component';
import { SimpleLayoutComponent } from './simple-layout/simple-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { GeneralLayoutComponent } from './general-layout/general-layout.component';

export {
  FullLayoutComponent,
  SimpleLayoutComponent,
  LoginLayoutComponent,
  GeneralLayoutComponent
};

export default {
  FullLayoutComponent,
  SimpleLayoutComponent,
  LoginLayoutComponent,
  GeneralLayoutComponent
};
