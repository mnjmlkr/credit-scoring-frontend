export interface GenericResponse<T = any> {
    success: boolean;
    message: string;
    code?: string | number;
    data?: T;
    [key: string]: any;
}
