import { Deserializable } from './deserializable.model';

export class NavigationModel implements Deserializable {
  name: string;
  navigation: string;
  role: string[] = [];
  icon: string;
  children?: NavigationModel[];
  anonymous = false;
  expand = false;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

  matchEvery(newNavigationModel: NavigationModel): boolean {
      if (!this._compareModel(newNavigationModel)) {
          return false;
      }
      if (this.children && this.children.length) {
        return this.children.every((child) => {
          return !!newNavigationModel.children.find((newChild) => child._compareModel(newChild));
        });
      }
      return true;
  }

  private _compareModel(newNavigationModel: NavigationModel) {
    return this.name === newNavigationModel.name
      && this.navigation === newNavigationModel.navigation
      && this.role === newNavigationModel.role
      && this.icon === newNavigationModel.icon
      && this.anonymous === newNavigationModel.anonymous;
  }
}
