import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { CredentialsService } from '../services/credentials.service';
import { PermissionService } from '../services/permission.service';
import { CSAppRoutesConstants } from '../../shared/constants';


@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(
    private router: Router,
    private credentialsService: CredentialsService,
    private permissionsService: PermissionService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.credentialsService.isAuthenticated()) {
      this.permissionsService.setPermissionsFromSession();
      return true;
    }

    this.router.navigate([CSAppRoutesConstants.LOGIN], { queryParams: { redirect: state.url }, replaceUrl: true });
    return false;
  }
}
