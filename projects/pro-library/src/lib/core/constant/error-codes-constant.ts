export class ErrorCodesConstant {
    public static readonly TOKEN_EXPIRED = 401;
    public static readonly INVALID_PAYLOAD = 402;
    public static readonly SESSION_CHANGED = 403;
    public static readonly INVALID_TOKEN = 404;
}
