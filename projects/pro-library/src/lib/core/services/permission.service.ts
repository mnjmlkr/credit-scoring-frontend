import { Injectable } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(
    private ngxPermissionsService: NgxPermissionsService
  ) { }

  setPermissions(permissions: string[]) {
    this.ngxPermissionsService.loadPermissions(permissions);
    localStorage.setItem('permissions', JSON.stringify(permissions));
  }

  setPermissionsFromSession() {
    if (localStorage.getItem('permissions')) {
      this.ngxPermissionsService.loadPermissions(JSON.parse(localStorage.getItem('permissions')));
    }
  }

  flushPermissions() {
    this.ngxPermissionsService.flushPermissions();
    if (localStorage.getItem('permissions')) {
      localStorage.removeItem('permissions');
    }
  }
}
