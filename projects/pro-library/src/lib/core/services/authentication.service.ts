import { HttpClient } from '@angular/common/http';
import { Injectable, Inject, Optional } from '@angular/core';
import { GenericResponse } from '../../shared/models';
import { CredentialsService } from './credentials.service';
import { Router } from '@angular/router';
import { PermissionService } from './permission.service';
import { CSApiConstants } from '../../shared/constants';

export interface AuthenticationCredentials {
    email: string;
    password: string;
}

/**
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    constructor(
        private credentialsService: CredentialsService,
        private http: HttpClient,
        private router: Router,
        private permissionsService: PermissionService,
        @Inject('endpoint') @Optional() public endpoint?: string,
    ) {
        this.endpoint = endpoint;
    }

    /**
     * Authenticates the user.
     * @param credentials The login parameters.
     * @return The user credentials.
     */
    login(credentials: AuthenticationCredentials) {
        return this.http.post<GenericResponse>(
            CSApiConstants.generateWebPath(this.endpoint,
                CSApiConstants.AUTH,
                CSApiConstants.LOGIN),
            credentials
        );
    }

    /**
     * Logs out the user and clear credentials.
     * @return True if the user was logged out successfully.
     */
    logout() {
        // Customize credentials invalidation here
        this.http.post<GenericResponse>(
            CSApiConstants.generateWebPath(this.endpoint,
                CSApiConstants.AUTH,
                CSApiConstants.LOGOUT),
              {}
      ).subscribe((successResponse) => {
        this.removeCredentials();
      }, (errorResponse) => {
        this.removeCredentials();
      });
    }

    removeCredentials() {
      this.credentialsService.credentials = null;
      this.permissionsService.flushPermissions();
      this.router.navigateByUrl('/login');
    }
}
