﻿import { Injectable } from '@angular/core';

export interface Credentials {
    token: string;
    email?: string;
    name?: string;
}

const CREDENTIALS_KEY = 'X-Authorization';

/**
 * Provides storage for authentication credentials.
 */
@Injectable({
    providedIn: 'root'
})
export class CredentialsService {
    private savedCredentials: Credentials | null = null;

    constructor() {
        const savedCredentials = localStorage.getItem(CREDENTIALS_KEY);
        if (savedCredentials) {
            this.savedCredentials = JSON.parse(savedCredentials);
        }
    }

    /**
     * Checks is the user is authenticated.
     * @return True if the user is authenticated.
     */
    isAuthenticated(): boolean {
        return !!this.savedCredentials;
    }

    /**
     * Gets the user credentials.
     * @return The user credentials or null if the user is not authenticated.
     */
    get credentials(): Credentials | null {
        return this.savedCredentials;
    }

    /**
     * Sets the user credentials.
     * The credentials are only persisted for the current session.
     * @param credentials The user credentials.
     */
    set credentials(credentials: Credentials) {
        this.savedCredentials = credentials;
        if (credentials) {
            localStorage.setItem(CREDENTIALS_KEY, JSON.stringify(this.savedCredentials));
        } else {
            localStorage.removeItem(CREDENTIALS_KEY);
        }
    }
}
