import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CredentialsService } from './services/credentials.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthTokenInterceptor } from './interceptors/auth-token.interceptor';
import { ErrorHandlerInterceptor } from './interceptors/error-handler.interceptor';
import { AuthenticationGuard } from './route-guards';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      HttpClientModule
  ],
  providers: [
      CredentialsService,
      AuthenticationService,
      {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthTokenInterceptor,
          multi: true,
      },
      {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorHandlerInterceptor,
          multi: true,
      },
      AuthenticationGuard,
  ],
  exports: [
    HttpClientModule
  ]
})
export class CoreModule { }
