import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { ErrorCodesConstant } from '../constant';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router

    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err: HttpErrorResponse) => {
            if (err.status === 401) {
                if (err.error.errorCode) {
                    const errorCode = err.error.errorCode;
                    if (errorCode === ErrorCodesConstant.TOKEN_EXPIRED) {
                        console.log('Please login again.', 'Session Expired');
                    } else if (errorCode === ErrorCodesConstant.INVALID_PAYLOAD) {
                        console.log('System has been updated. Please login again.', 'Session Logged Out');
                    } else if (errorCode === ErrorCodesConstant.SESSION_CHANGED) {
                        console.log('Your account has been logged in from another system.', 'Session Logged Out');
                    } else if (errorCode === ErrorCodesConstant.INVALID_TOKEN) {
                        console.log('Please login again.', 'Session Logged Out');
                    } else {
                        console.log(err.error.message);
                    }
                } else {
                    console.log(err.error.message);
                }
                // auto logout if 401 response returned from api
                this.authenticationService.removeCredentials();
                return EMPTY;
            }
            // tslint:disable-next-line: no-commented-code
            // const error = err.error.message || err.statusText;
            return throwError(err);
        }));
    }
}
