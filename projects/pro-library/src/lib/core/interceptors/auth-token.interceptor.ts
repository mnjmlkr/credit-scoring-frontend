import { HttpEvent, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CredentialsService } from '../services/credentials.service';
import { CSAppConstants } from '../../shared/constants';

@Injectable({
  providedIn: 'root'
})export class AuthTokenInterceptor {
    constructor(
        private credential: CredentialsService,
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Sets authorization token in the request header if present
        const token = this.credential.isAuthenticated() ? this.credential.credentials.token : '';
        const header = token ? { 'X-Authorization': `Bearer ${token}` } : {};
        const clonedRequest: HttpRequest<any> = request.clone({
            setHeaders: header,
        });

        return next.handle(clonedRequest).pipe(
            // Update the authorization token in the session storage from response header
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    const responseToken = event.headers.get(CSAppConstants.SESSION.AUTHORIZATION);
                    if (responseToken && token !== responseToken) {
                        this.credential.credentials = {
                            ... this.credential.credentials,
                            token: responseToken,
                        };
                    }
                }
                return event;
            }),
        );
    }
}
